import PropTypes from "prop-types"
import React, { useState } from "react"
import styles from "./typewriter.module.css"

const Typewriter = ({ text }) => {
  return (
    <div className={styles.typewriter}>
      <h1>{text}</h1>
    </div>
  )
}

export default Typewriter
