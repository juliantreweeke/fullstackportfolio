import PropTypes from "prop-types"
import React from "react"
import styles from "./button.module.css"

const Button = ({ link, text }) => {
  return (
    <button className={styles.button}>
      {link ? (
        <a
          className={styles.link}
          href={link}
          target="_blank"
          rel="noopener noreferrer"
        >
          {text}
        </a>
      ) : (
        text
      )}
    </button>
  )
}

Button.propTypes = {
  link: PropTypes.string,
  text: PropTypes.string.isRequired,
}

export default Button
