import PropTypes from "prop-types"
import React, { useState } from "react"
import styles from "./menu.module.css"
import { Link } from "gatsby"

const Menu = ({ isMenuOpen, toggleMenuOpen }) => {
//   const [menuOpen, setMenuOpen] = useState(false)

//   const toggleMenuOpen = () => {
//     setMenuOpen(!menuOpen)
//   }

  return (
    <ul className={styles.menu}
      data-is-menu-open={isMenuOpen}
    > 
      <li onClick={toggleMenuOpen}><Link to="/">HOME</Link></li>
      <li onClick={toggleMenuOpen}><Link to="/about">ABOUT</Link></li>
      <li onClick={toggleMenuOpen}><Link to="/projects">PROJECTS</Link></li>
      <li onClick={toggleMenuOpen}><Link to="/contact">CONTACT</Link></li>
    </ul>
  )
}

export default Menu
