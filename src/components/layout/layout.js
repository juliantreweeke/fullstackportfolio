/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import Menu from "../menu/menu"
import Header from "../header/header"
import "./layout.css"

const Layout = ({ children, hasAnimation }) => {
  const [isMenuOpen, setIsMenuOpen] = useState(false)

  const [hasLoaded, setHasLoaded] = useState(false)

  useEffect(() => {
    setHasLoaded(true)
  }, [])

  const toggleMenuOpen = () => {
    setIsMenuOpen(!isMenuOpen)
  }

  return (
    <div data-has-loaded={hasLoaded} className="layout">
      <Menu isMenuOpen={isMenuOpen} toggleMenuOpen={toggleMenuOpen} />
      <Header
        hasAnimation={hasAnimation}
        isMenuOpen={isMenuOpen}
        toggleMenuOpen={toggleMenuOpen}
      />
      <div className="container">
        {children}
        <footer></footer>
      </div>
    </div>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
