import React, { useState } from "react"
import styles from "./wavingJulian.module.css"

const WavingJulian = () => {
  const [julianClicked, setJulianClicked] = useState(false)

  const toggleJulianClicked = () => {
    setJulianClicked(true)
  }

  return (
    <div className={`${julianClicked ? styles.byeJulian : ""}`}>
      <div onClick={toggleJulianClicked} className={styles.wavingJulian} />
    </div>
  )
}

export default WavingJulian
