import PropTypes from "prop-types"
import React from "react"
import styles from "./arrow.module.css"

const Arrow = ({children, direction}) => {
  return (
      <i className={`${styles.arrow} ${styles[direction] }`}>
        {children}
      </i>
  )
}

Arrow.propTypes = {
  children: PropTypes.node,
  direction: PropTypes.string.isRequired,
}

export default Arrow

