import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import Burger from "../burger/burger"
import styles from "./header.module.css"

const Header = ({ hasAnimation, isMenuOpen, toggleMenuOpen}) => (
  <header
    className={styles.header}
    data-is-menu-open={isMenuOpen}
  >
    <div className={styles.headingContainer}>
      <h1 className={styles.heading} data-has-animation={hasAnimation}>
        <Link
          to="/"
          style={{
            color: `white`,
            textDecoration: `none`,
          }}
        >
          Julian Treweeke
        </Link>
      </h1>
        <Burger isMenuOpen={isMenuOpen} toggleMenuOpen={toggleMenuOpen}/>
    </div>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
