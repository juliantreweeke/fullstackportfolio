import PropTypes from "prop-types"
import React from "react"
import styles from "./project.module.css"
import Layout from "../layout/layout"
import Button from "../button/button"
import Arrow from "../arrow/arrow"
import SEO from "../seo"
import { Link } from "gatsby"
import { projects } from "../../data"

const Project = ({ pageContext: { route } }) => {
  const data = projects.find(project => project.route === route)
  const dataIndex = projects.findIndex(project => project.route === route)
  const lastProjectIndex = projects.length - 1
  const previousProject =
    dataIndex === 0
      ? `/${projects[lastProjectIndex].route}`
      : `/${projects[dataIndex - 1].route}`
  const nextProject =
    dataIndex === lastProjectIndex
      ? `/${projects[0].route}`
      : `/${projects[dataIndex + 1].route}`

  const { description, link, learnings, heading, image, tags } = data
  return (
    <Layout>
      <SEO title={heading} />
      <div className={styles.project}>
        <h1 className={styles.projectHeading}>{heading}</h1>
        <h2 className={styles.projectHeading}>Technologies used</h2>
        <ul className={styles.tagListContainer}>
          {tags.map((tag, index) => (
            <li key={index} className={styles.tag}>
              {tag}
            </li>
          ))}
        </ul>
        <h2 className={styles.projectHeading}>Description</h2>
        <p className={styles.projectText}>{description}</p>
        <h2 className={styles.projectHeading}>What I learnt</h2>
        <p className={styles.projectText}>{learnings}</p>
        <Button text={"VIEW SITE"} link={link} />
        <img className={styles.projectImage} src={image} />
        <Link
          to={previousProject}
          className={`${styles.arrow} ${styles.arrowLeft}`}
        >
          <Arrow direction="left" />
        </Link>
        <Link
          to={nextProject}
          className={`${styles.arrow} ${styles.arrowRight}`}
        >
          <Arrow direction="right" />
        </Link>
      </div>
    </Layout>
  )
}

Project.propTypes = {
  route: PropTypes.string.isRequired,
}

export default Project
