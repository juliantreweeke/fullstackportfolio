import PropTypes from "prop-types"
import React, { useState } from "react"
import styles from "./burger.module.css"

const Burger = ({ isMenuOpen, toggleMenuOpen }) => {
  return (
    <div
      data-is-menu-open={isMenuOpen}
      className={styles.burger}
      onClick={toggleMenuOpen}
    >
      <i className={styles.burgerLine}></i>
      <i className={styles.burgerLine}></i>
      <i className={styles.burgerLine}></i>
    </div>
  )
}

export default Burger
