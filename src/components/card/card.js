import PropTypes from "prop-types"
import React from "react"
import styles from "./card.module.css"
import { Link } from "gatsby"

const Card = ({ heading, image, route, tags }) => {
  return (
    <div className={styles.card}>
      <Link className={styles.cardLink} to={`/${route}`}>
        <div className={styles.header}>
          <h3 className={styles.heading}>{heading}</h3>
        </div>
        <img className={styles.cardImage} src={image} alt={heading} />
        <ul className={styles.tagListContainer}>
          {tags.map((tag, index) => (
            <li key={index} className={styles.tag}>
              {tag}
            </li>
          ))}
        </ul>
      </Link>
    </div>
  )
}

Card.propTypes = {
  heading: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  route: PropTypes.string.isRequired,
  tags: PropTypes.string.isRequired,
}

export default Card
