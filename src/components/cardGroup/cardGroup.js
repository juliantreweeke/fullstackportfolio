import PropTypes from "prop-types"
import React from "react"
import styles from "./cardGroup.module.css"
import Card from "../card/card"

const CardGroup = ({ cards }) => {
  return (
      <div className={styles.cardGroup}>
        <div className={styles.cards}>
          {cards.map((card, index) => {
            return (
              <Card
                key={index}
                image={card.image}
                heading={card.heading}
                route={card.route}
                tags={card.tags}
              />
            )
          })}
        </div>
      </div>
  )
}

export default CardGroup
