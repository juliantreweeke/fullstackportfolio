import React from "react"
import styles from "./landing.module.css"
import Typewriter from "../typewriter/typewriter"
import CardGroup from "../cardGroup/cardGroup"
import WavingJulian from "../wavingJulian/wavingJulian"

const Landing = ({ projects }) => (
  <>
    <Typewriter text="Hi, I'm a Full Stack Web Developer." />
    <WavingJulian />
    <h2 className={styles.projectsHeading}>Here are some of my projects</h2>
    <CardGroup cards={projects} delay={2000} />
  </>
)

export default Landing
