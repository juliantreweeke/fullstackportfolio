import PropTypes from "prop-types"
import React, { useState } from "react"
import styles from "./slideRightHeading.module.css"

const SlideRightHeading = ({ text }) => {
  const textArray = [...text];
  return (
    <div className={styles.slideRightHeading}>
      <h1>{textArray.map((letter,index) => <span className={styles.letter} key={index}>{letter}</span>)}</h1>
    </div>
  )
}

export default SlideRightHeading
