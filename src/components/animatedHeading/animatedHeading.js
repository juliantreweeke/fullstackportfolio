import PropTypes from "prop-types"
import React from "react"
import styles from "./animatedHeading.module.css"

const AnimatedHeading = ({ text, drop, spring }) => {
  const textArray = [...text]
  return (
    <div className={styles.animatedHeading}>
      <h1>
        {textArray.map((character, index) => (
          <span
            className={`${styles.character} 
            ${drop ? styles.drop : ""}
            ${spring ? styles.spring : ""} 
            `}
            key={index}
          >
            {character}
          </span>
        ))}
      </h1>
    </div>
  )
}

AnimatedHeading.propTypes = {
  drop: PropTypes.bool,
  spring: PropTypes.bool,
  text: PropTypes.string.isRequired,
}

export default AnimatedHeading
