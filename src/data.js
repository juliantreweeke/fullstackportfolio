import DysphemicImage from "./images/dysphemic.jpg"
import HappyDoggoImage from "./images/happydoggo.jpg"
import NaturopathImage from "./images/naturopath.jpg"
import MusicPlayerImage from "./images/musicplayer.jpg"

export const about = {
  description:'Hi, my name is Julian and I am a developer based in Sydney.'
}

export const projects = [
  {
    heading: "MUSIC PLAYER",
    tags: ["REACT", "REDUX", "REACT SPRING", "SOUNDCLOUD API"],
    image: MusicPlayerImage,
    route:'music-player',
    link:'#',
    description:"This is a project that is continuous a WIP. At my last role we changed from React to Vue, so I kept this repo alive as sandbox to stay up to date with React. As part of this project I converted React and Redux to use hooks. It also was an experimentaion with using React Spring for animations.",
    learnings:"I learnt that doing in animationss in React feels very cumbersome and at times very complex. After this project I now know I prefer to do animations in pure CSS"
  },
  {
    heading: "MUSIC PRODUCER",
    tags: [
      "GATSBY",
      "REACT",
      "TYPESCRIPT",
      "LOG ROCKET",
      "EMAIL MARKETING",
      "WEB DESIGN",
    ],
    image: DysphemicImage,
    route:'music-producer',
    link:'https://www.dysphemic.com/',
    description:'This is a website that I use for my music side project. It is a relatively simple site where all traffic from social media, blogs, online press and email marketing campaigns are funnelled into this website. I also have a email list of 25k+ fanbase I have gathered over the years that I frequently send to this website. This site is a great side project to keep my skills sharp and to have that real world experience of having real consequences to a code base. If there is a bug, I potentially will lose money from marketing. It is a work in progress that is constantly evolving.',
    learnings:"This is the first site I made in Gatsby and made me fall in love with the ease of developer experience of the framework. I also really liked building this without a css framework, using straight up sass was so refreshing and makes it super easy to maintain."
  },
  {
    heading: "HAPPY DOGGO",
    tags: [
      "WORD PRESS",
      "WEB DESIGN",
      "LOGO DESIGN",
      "CSS ANIMATIONS",
      "PHOTOSHOP",
    ],
    image: HappyDoggoImage,
    route:'happy-doggo',
    link:'https://thehappydoggo.com/',
    description:'This is a freelance project I did for a drop shipping store in Wordpress',
    learnings:"It was good to return to wordpress after years of not using in, and finding out it is not as painful as I once thought. Also I really enjoyed animating the dog with css and playing around in photoshop with designing the logo."
  },
  {
    heading: "NATUROPATH",
    tags: ["GATSBY", "REACT", "GRAPHQL", "CONTENTFUL CMS"],
    image: NaturopathImage,
    route:'naturopath',
    link:'https://www.amandalanenaturopath.com/',
    description:'This is a freelance project I did for a naturopath business, working off requirements from a designer.',
    learnings:"It was great to learn contentful CMS and the main challenge for this project was getting formatting text to be rendered in an elegent way. I also tried Rebass as the component library which was interesting as it gave ideas and inspiration for building my own in the future. I still prefer making everything from scratch, but I can see the benefit of using such a library once you get used to it and have used it for a few projects. This project also gave me the opportunity to get more familiar with GraphQL"
  },
]



