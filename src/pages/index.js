import React from "react"
import Landing from "../components/landing/landing"
import Layout from "../components/layout/layout"
import SEO from "../components/seo"
import { projects }from '../data';

const IndexPage = () => {
  return (
    <Layout hasAnimation={true}>
      <SEO title="Home" />
      <Landing projects={projects}/>
    </Layout>
  )
}

export default IndexPage
