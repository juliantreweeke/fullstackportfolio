import React from "react"
import CardGroup from "../components/cardGroup/cardGroup"
import Layout from "../components/layout/layout"
import AnimatedHeading from "../components/animatedHeading/animatedHeading"
import SEO from "../components/seo"
import { projects } from '../data';

const Projects = () => {
  return (
    <Layout>
      <SEO title="Projects" />
      <AnimatedHeading text="Projects" spring />
      <CardGroup cards={projects} delay={2000}/>
    </Layout>
  )
}

export default Projects
