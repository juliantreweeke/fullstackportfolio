import React from "react"
import AnimatedHeading from "../components/animatedHeading/animatedHeading"
import Layout from "../components/layout/layout"
import SEO from "../components/seo"

const About = () => {
  return (
    <Layout>
      <SEO title="About"/>
      <AnimatedHeading text="About me" drop />
    </Layout>
  )
}

export default About
