/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it
// import projects from './src/data';

// const projectRoutes = [
//     {
//       route:'music-producer'
//     },
//     {
//       route:'music-player'
//     },
//     {
//       route:'naturopath'
//     },
//     {
//       route:'happy-doggo'
//     }
//   ];

  const projectRoutes = ['music-producer','music-player','naturopath','happy-doggo'];
  
  exports.createPages = ({ actions: { createPage } }) => {
    projectRoutes.forEach(route => {
      createPage({
        path: `${route}/`,
        component: require.resolve('./src/components/project/project'),
        context: {
          route,
        },
      });
    });
  };